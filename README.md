# REPOSITORY MOVED

## This repository has moved to

https://github.com/osrf/mesh_to_sdf

## Issues and pull requests are backed up at

https://osrf-migration.github.io/osrf-others-gh-pages/#!/osrf/mesh_to_sdf

## Until BitBucket removes Mercurial support, this read-only mercurial repository will be at

https://bitbucket.org/osrf-migrated/mesh_to_sdf

## More info at

https://community.gazebosim.org/t/important-gazebo-and-ignition-are-going-to-github/533

